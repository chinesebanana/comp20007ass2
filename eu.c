#include <stdio.h>
#include <stdlib.h>

int euclidean(int x, int y) { //do i need size? INITIAL a = 1
    //xs0 + ksize = 1
    //x > y, put a check in the outside function
    int next_x, next_y;

    if (y == 1) {
        return 1;
    }

    if (x == 1) {
        return 0;
    }

    next_x = y;
    next_y = x % y;
    //printf("%d %d %d\n", x, y, (x-(x%y))/y);

    int nextnext_eu = euclidean(next_y, next_x % next_y);
    int next_eu = euclidean(y, x % y);

    //printf("nexts: %d -%d*%d\n", nextnext_eu, (x-(x%y))/y, next_eu);
    return nextnext_eu - (x-(x%y))/y * next_eu;
}

int main(int argc, char const *argv[]) {
    int x = 23, y = 20, ans;
    ans = euclidean(x, y);
    printf("ans: %d\n", ans);
    if (ans < 0) {
        ans += x;
        printf("neg: %d\n", ans);
    }
    printf("check: %d\n", (ans*y) % x);
    return 0;
}
