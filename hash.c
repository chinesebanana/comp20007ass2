/*
 * COMP20007 Design of Algorithms
 * Semester 1 2016
 * Assignment 2
 *
 * Joanna Grace Cho Ern LEE (student no.: 710094)
 *
 * This module implements various hashing algorithms
 *
*/
#include "hash.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAXSTRLEN 256

/* Used as the second hashing function on double hash */
unsigned int linear_probe(void *e, unsigned int size) {
    // returns step size of linear probing
    return 1;
}

/* Very simple hash */
unsigned int worst_hash(void *e, unsigned int size) {
    (void) e;
    (void) size;
    return 0;
}

/* Basic numeric hash function */
unsigned int num_hash(long n, unsigned int size) {
    return n % size;
}

/* Bad hash function */
unsigned int bad_hash(char *key, unsigned int size) {
    static unsigned int a;
    static bool generated = false;
    if (generated == false) {
        a = rand() % size;
        generated = true;
    }
    return a * key[0] % size;
}

/* Universal hash function as described in Dasgupta et al 1.5.2 */
unsigned int universal_hash(unsigned char *key, unsigned int size) {
    unsigned int sum=0;
    int i=0;
    static int R[MAXSTRLEN]; //so will return same hash for the same inputs
    static bool generated = false;

    if (generated == false) {
        for (int k=0; k<MAXSTRLEN; k++) {
            R[k] = rand() % size;
            while (R[k] < 1) {
                R[k] = rand() % size;
            }
        }
        generated = true;
    }

    while (key[i]) {
        sum += (R[i]) * key[i];
        i++;
    }
    return sum % size;
}
