/*
 * COMP20007 Design of Algorithms
 * Semester 1 2016
 * Assignment 2
 *
 * Joanna Grace Cho Ern LEE (student no.: 710094)
 *
 * This module implements various collide algorithms and prime number generator
 *
*/
#include "extra.h"
#include "hash.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <string.h>

#define MAXSTRLEN 256
#define COLEXTRA 0

/* Determine appropriate size of a hash table given input size n */
unsigned int determine_size(unsigned int n) {
    unsigned int prime = (2*n)+1, isprime = 0, divisor, broke;
    while (isprime==0) {
        for (divisor=2; divisor*divisor<=prime; divisor++) {
            if (prime%divisor==0) {
                broke = 1;
                break;
            }
        }
        if (broke==1) {
            prime++;
            broke = 0;
        } else {
            isprime=1;
        }
    }
    return prime;
}

/* Returns string made up of random alphanumeric characters with length len */
char *random_string(const int len) {
    char *s = (char *) malloc(MAXSTRLEN+1);
    assert(s);
    char alphanum[] =
        "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    for (int i = 0; i < len; ++i) {
        s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
    }

    s[len-1] = '\0';
    return s;
}

/* Print n strings that are hashed to 0 by universal_hash seeded with seed */
void collide_dumb(unsigned int size, unsigned int seed, int n) {
    int count = 0, len;
    char *str = (char *) malloc(MAXSTRLEN+1);
    assert(str);
    srand(seed);

    /* RANDOM NUMBER GENERATOR */
    int R[MAXSTRLEN], L[n], max = 0;;
    for (int i=0; i<n; i++) {
        L[i] = rand() % size;
        while (L[i] < 1) { //ensure valid length
            L[i] = rand() % size;
        }
        R[i] = L[i];
        if (L[i] > max) {
            max = L[i];
        }
        if (max < n) {
            printf("%d\n", n);
        } else {
            printf("%d\n", max);
        }
        printf("%d\n", R[i]);
    }
    if (max > n) {  //only generate random numbers that are needed
        for (int k=n; k<max; k++) {
            R[k] = rand() % size;
            while (R[k] < 1) {
                R[k] = rand() % size;
            }
            printf("%d\n", R[k]);
        }
    }

    /* RANDOM STRING GENERATOR */
    while (count < n) {
        len = rand() % MAXSTRLEN;
        while (len <= 1) {
            len = rand() % MAXSTRLEN;
        }
        strcpy(str, random_string(len));
        if (universal_hash(str, size) == 0) {
            printf("%s\n", str);
            count++;
        }
    }
    free(str);
    //check if repeats, array of strings
}

/* In order to make the sum of all r[i]s[i] values to be 0 mod size,
 * this function makes all the s[i] values to be size */
void collide_extra(unsigned int size, unsigned int seed, int n) {
    if (n > MAXSTRLEN) {
        fprintf(stderr,"COLLIDE CLEVER DOES NOT WORK FOR n > %d.\n",MAXSTRLEN);
       exit(EXIT_FAILURE);
    }
    if (size > 254 || size < 32) {
        fprintf(stderr, "COLLIDE CLEVER DOES NOT WORK FOR THIS size VALUE.\n");
       exit(EXIT_FAILURE);
    }

    char *str = (char *) malloc(n + 1);
    assert(str);
    srand(seed);
    int R[n], i;

    for (int k=0; k<n; k++) {
        R[k] = rand() % size;
        printf("%d\n", R[k]);
    }
    for (i=0; i<n; i++) {
        str[i] = (char) size;
        while (str[i] < 32) {
            str[i] += size;
        }
        str[i+1] = '\0';
        printf("%s\n", str);
    }
    free(str);
}

int euclidean(int x, int y) {
    //xs0 + ksize = 1
    if (y == 1)
        return 1;
    if (x == 1 || y == 0)
        return 0;

    int next_x, next_y;
    next_x = y;
    next_y = x % y;

    int nextnext_eu = euclidean(next_y, next_x % next_y);
    int next_eu = euclidean(y, x % y);

    return nextnext_eu - (x-(x%y))/y * next_eu;
}

/* Print n strings that are hashed to 0 by universal_hash seeded with seed */
void collide_clever(unsigned int size, unsigned int seed, int n) {
    if (n > MAXSTRLEN) {
        fprintf(stderr,"COLLIDE CLEVER DOES NOT WORK FOR n > %d.\n",MAXSTRLEN);
       exit(EXIT_FAILURE);
    }
    if (size > MAXSTRLEN) {
        fprintf(stderr,
            "COLLIDE CLEVER DOES NOT WORK FOR size > %d.\n",MAXSTRLEN);
       exit(EXIT_FAILURE);
    }
    if (COLEXTRA) { // Other implementation I wrote that is a bit hacky.
        collide_extra(size, seed, n);
    } else {
        srand(seed);
        int R[n], S[n];
        printf("%d\n", n);
        /* RANDOM NUMBER GENERATOR */
        for (int i=0; i<n; i++) {
            R[i] = rand() % size;
            while (R[i] < 1) {
                R[i] = rand() % size;
            }
            S[i] = euclidean(size, R[i]);
            printf("%d\n", R[i]);
        }

        /* GETTING VALUE OF S[i] */
        int s, last, len=1;
        while (len < n) {
            bool end = false;
            last = size - len + 1;
            // The other s[i]r[i] values remain 1.
            for (int j=0; j<len; j++) {
                s = S[j];
                if (j==len-1) {
                    s = s * last;
                    end = true;
                }
                while (s<32) {
                    s += size;
                    /* size is too large for c to fit in range */
                    if (!end && (s>254 || s==127)) {
                        /* if S[i] can't be valid by just adding size,
                         * end the string, with calculated value. */
                        s = s * (size - j)%size;
                        end = true;
                        break;
                    }
                }
                while (s>254) {
                    s -= size;
                    /* size is too large for c to fit in range :*/
                    if (!end && (s<32 || s==127)) {

                        /* if S[i] can't be valid by just subtracting size,
                         * end the string, with calculated value. */
                        s = s * (size - j)%size;
                        end = true;
                        break;
                    }
                }
                printf("%c", s);
                if (end) {
                    break;
                }
            }
            printf("\n");
            len++;
        }
    }
}
